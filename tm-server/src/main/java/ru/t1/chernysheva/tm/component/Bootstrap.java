package ru.t1.chernysheva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.endpoint.*;
import ru.t1.chernysheva.tm.api.service.*;
import ru.t1.chernysheva.tm.api.service.dto.*;
import ru.t1.chernysheva.tm.api.service.model.IProjectService;
import ru.t1.chernysheva.tm.api.service.model.IProjectTaskService;
import ru.t1.chernysheva.tm.api.service.model.ITaskService;
import ru.t1.chernysheva.tm.api.service.model.IUserService;
import ru.t1.chernysheva.tm.endpoint.*;
import ru.t1.chernysheva.tm.service.*;
import ru.t1.chernysheva.tm.service.dto.*;
import ru.t1.chernysheva.tm.service.model.ProjectService;
import ru.t1.chernysheva.tm.service.model.ProjectTaskService;
import ru.t1.chernysheva.tm.service.model.TaskService;
import ru.t1.chernysheva.tm.service.model.UserService;
import ru.t1.chernysheva.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    @Getter
    private final IProjectDtoService projectDtoService = new ProjectDtoService(connectionService);

    @NotNull
    @Getter
    private final ITaskDtoService taskDtoService = new TaskDtoService(connectionService);

    @NotNull
    @Getter
    private final IUserDtoService userDtoService = new UserDtoService(propertyService, connectionService);

    @NotNull
    @Getter
    private final IProjectTaskDtoService projectTaskDtoService = new ProjectTaskDtoService(connectionService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService, connectionService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final ISessionDtoService sessionService = new SessionDtoService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userDtoService, sessionService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String port = getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    public void start() {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);


        initPID();

        loggerService.initJmsLogger();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));

    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}

