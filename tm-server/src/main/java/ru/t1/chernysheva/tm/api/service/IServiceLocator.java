package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.service.dto.IProjectDtoService;
import ru.t1.chernysheva.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.chernysheva.tm.api.service.dto.ITaskDtoService;
import ru.t1.chernysheva.tm.api.service.dto.IUserDtoService;
import ru.t1.chernysheva.tm.api.service.model.IProjectService;
import ru.t1.chernysheva.tm.api.service.model.IProjectTaskService;
import ru.t1.chernysheva.tm.api.service.model.ITaskService;
import ru.t1.chernysheva.tm.api.service.model.IUserService;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IProjectDtoService getProjectDtoService();

    @NotNull
    IProjectTaskDtoService getProjectTaskDtoService();

    @NotNull
    ITaskDtoService getTaskDtoService();

    @NotNull
    IUserDtoService getUserDtoService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

}
