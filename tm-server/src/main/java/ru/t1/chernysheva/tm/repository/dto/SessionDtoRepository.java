package ru.t1.chernysheva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.chernysheva.tm.comparator.CreatedComparator;
import ru.t1.chernysheva.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;

public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> implements ISessionDtoRepository {

    public SessionDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<SessionDTO> getEntityClass() {
        return SessionDTO.class;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.find(getEntityClass(), id) != null;
    }

    @Override
    public void remove(@NotNull final SessionDTO model) {
        entityManager.remove(model);
    }

}


