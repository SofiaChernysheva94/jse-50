package ru.t1.chernysheva.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
