package ru.t1.chernysheva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.repository.model.ISessionRepository;
import ru.t1.chernysheva.tm.comparator.CreatedComparator;
import ru.t1.chernysheva.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.Comparator;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<Session> getEntityClass() {
        return Session.class;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.find(getEntityClass(), id) != null;
    }

    @Override
    public void remove(@NotNull final Session model) {
        entityManager.remove(model);
    }

}

