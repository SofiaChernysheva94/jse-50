package ru.t1.chernysheva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.model.IWBS;
import ru.t1.chernysheva.tm.enumerated.Status;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_project")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @Basic
    @Column(name = "description")
    private String description;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

}
