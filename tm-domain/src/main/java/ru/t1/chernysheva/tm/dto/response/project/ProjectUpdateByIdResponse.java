package ru.t1.chernysheva.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectUpdateByIdResponse extends AbstractProjectResponse {

    public ProjectUpdateByIdResponse(@NotNull final ProjectDTO project) {
        super(project);
    }

}
