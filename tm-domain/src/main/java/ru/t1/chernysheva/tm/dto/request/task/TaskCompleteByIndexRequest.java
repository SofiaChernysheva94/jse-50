package ru.t1.chernysheva.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskCompleteByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public TaskCompleteByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
