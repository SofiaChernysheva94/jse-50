package ru.t1.chernysheva.tm.exception.entity;

public class EntityNotFoundException extends AbstractEntityException {

    public EntityNotFoundException() {
        super("Error! Object is not exist...");
    }

}
