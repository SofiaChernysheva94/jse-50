package ru.t1.chernysheva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public final class UserLogoutResponse extends AbstractResultResponse {
}
