package ru.t1.chernysheva.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectStartByIdResponse extends AbstractProjectResponse {

    public ProjectStartByIdResponse(@NotNull final ProjectDTO project) {
        super(project);
    }

}
