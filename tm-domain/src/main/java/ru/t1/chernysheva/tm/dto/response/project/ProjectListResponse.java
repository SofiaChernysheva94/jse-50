package ru.t1.chernysheva.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;

import java.util.List;

@NoArgsConstructor
public final class ProjectListResponse extends AbstractProjectResponse {

    private List<ProjectDTO> projects;

    public ProjectListResponse(List<ProjectDTO> projects) {
        this.projects = projects;
    }

}
